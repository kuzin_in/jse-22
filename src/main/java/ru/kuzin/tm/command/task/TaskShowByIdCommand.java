package ru.kuzin.tm.command.task;

import ru.kuzin.tm.model.Task;
import ru.kuzin.tm.util.TerminalUtil;

public final class TaskShowByIdCommand extends AbstractTaskCommand{

    private static final String NAME = "task-show-by-id";

    private static final String DESCRIPTION = "Display task by id.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        final String userId = getUserId();
        System.out.println("[SHOW TASK BY ID");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = getTaskService().findOneById(userId, id);
        showTask(task);
    }

}