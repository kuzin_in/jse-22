package ru.kuzin.tm.command.user;

import ru.kuzin.tm.enumerated.Role;
import ru.kuzin.tm.util.TerminalUtil;

public final class UserRemoveCommand extends AbstractUserCommand {

    private final String NAME = "user-remove";

    private final String DESCRIPTION = "user remove";

    @Override
    public void execute() {
        System.out.println("[USER REMOVE]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        getUserService().lockUserByLogin(login);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}