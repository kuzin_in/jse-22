package ru.kuzin.tm.command.user;

import ru.kuzin.tm.enumerated.Role;
import ru.kuzin.tm.util.TerminalUtil;

public final class UserUnlockCommand extends AbstractUserCommand {

    private final String NAME = "user-unlock";

    private final String DESCRIPTION = "user unlock";

    @Override
    public void execute() {
        System.out.println("[USER UNLOCK]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        getUserService().unlockUserByLogin(login);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}