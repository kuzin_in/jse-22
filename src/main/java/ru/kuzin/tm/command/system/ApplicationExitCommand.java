package ru.kuzin.tm.command.system;

public final class ApplicationExitCommand extends AbstractSystemCommand {

    private static final String NAME = "exit";

    private static final String DESCRIPTION = "Close application.";

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}