package ru.kuzin.tm.api.repository;

import ru.kuzin.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    Task create(String userId, String name, String description);

    Task create(String userId, String name);

    List<Task> findAllByProjectId(String userId, String projectId);

}