package ru.kuzin.tm.api.service;

import ru.kuzin.tm.enumerated.Role;
import ru.kuzin.tm.model.User;

public interface IAuthService {

    void checkRoles(Role[] roles);

    User registry(String login, String password, String email);

    void login(String login, String password);

    void logout();

    boolean isAuth();

    String getUserId();

    User getUser();

}